package com.liveramp.uml2swagger;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({})
public class NameTests {

	@Test
	void testSanitize() {
		assertEquals("battery-hens", Helpers.sanitize("BatteryHens"));
	}
	
	@Test
	void testRemoveVerb() {
		assertEquals("my-amazing", Helpers.removeVerb("my-amazing-verbs"));		
		assertEquals("x", Helpers.removeVerb("x"));		
	}
}
