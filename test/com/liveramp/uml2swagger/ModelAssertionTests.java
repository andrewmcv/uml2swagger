package com.liveramp.uml2swagger;

import org.junit.jupiter.api.Test;

class ModelAssertionTests {
	private static String base = "./";
	

	@Test
	void testModelAssertions() {
		transformAndCompare("model-assertions");
	}
	
	private void transformAndCompare(String testCase) {
		Main.setModuleName("/com/liveramp/uml2swagger/modelAssertion");
		Main.main(new String[] {
				base + "models/uml-to-swagger-test.xmi",
				base + "tests/" + testCase
		});
		GenerationTests.compare(testCase, ".output");
	}
}
 