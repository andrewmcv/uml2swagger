package com.liveramp.uml2swagger;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

public class GenerationTests {
	private static String base = "./";
	

	@Test
	void testRequest() {
		transformAndCompare("request");
	}


	@Test
	void testSimpleResource() {
		transformAndCompare("simple-resource");
	}

	@Test
	void testComplexResource() {
		transformAndCompare("complex-resource");
	}
	
	@Test
	void testUpversion() {
		transformAndCompare("upversion");
	}
	
	@Test
	void testSingleton() {
		transformAndCompare("singleton");
	}

	private void transformAndCompare(String testCase) {
		Main.main(new String[] {
				base + "models/uml-to-swagger-test.xmi",
				base + "tests/" + testCase,
				base + "tests/" + testCase + "/test.properties"
		});
		compare(testCase, null);
	}
	
	public static void compare(String name, String extension) {
		String start = base + "tests/" + name + "/" + name;
		try {
			String expected = read(start + ".expected");
			String actual = read(start + (extension == null ? ".swagger" : extension));
			assertEquals(expected, actual);
		} catch (Exception ex) {
			ex.printStackTrace();
			assertTrue("Problem opening file: " + ex.getMessage(), false);
		}
	}
	
	/** read the file in and remove any empty lines to make the comparison more robust */
	public static String read(String fname) throws IOException {
		// read in the file and remove any trailing whitespace or empty lines
		return new String(Files.readAllBytes(Paths.get(fname)), StandardCharsets.UTF_8).
				replaceAll("(?m)^[ \t]*\r?\n|( *$)", "");
	}
}
 