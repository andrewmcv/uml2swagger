package com.liveramp.uml2swagger;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GenerationTests {

	@Test
	void testSimpleResource() {
		String base = "/Users/amcvei/git/repository/";
		Main.main(new String[] {
				base + "uml2swagger/models/uml-to-swagger-test.xmi",
				base + "uml2swagger/tests/simple-resource",
				base + "uml2swagger/tests/simple-resource/test.properties"
		});
		System.out.println("Finished generation");
	}

	@Test
	void testComplexResource() {
		String base = "/Users/amcvei/git/repository/";
		Main.main(new String[] {
				base + "uml2swagger/models/uml-to-swagger-test.xmi",
				base + "uml2swagger/tests/complex-resource",
				base + "uml2swagger/tests/complex-resource/test.properties"
		});
		System.out.println("Finished generation");
	}
}
 