---
swagger: "2.0"
info:
  title: "complex-resource"
  version: "1.0.0"
  description: ""
basePath: "/complex-resource"
schemes:
- "http"
- "https"
tags:
- name: "Specification"
  description: ""- name: "Wheel"
  description: ""- name: "Car"
  description: ""
paths:
    /complex-resource/v1/wheels/{id}:
        get:
            tags:
            - "Wheel"
            operationId: "retrieve_Wheel"
            
            parameters:
            - in: "path"
              name: "id"
              required: true
              type: "integer"
              format: "int32"
              
            responses:
                200:
                    description: "Wheel retrieved successfully"
                    schema:
                        $ref: "#/definitions/WheelBody"
    
        put:
            tags:
            - "Wheel"
            operationId: "modify_Wheel"
            
            parameters:
            - in: "path"
              name: "id"
              required: true
              type: "integer"
              format: "int32"
              
            - in: "body"
              name: "WheelRequestBody"
              schema:
                $ref: "#/definitions/WheelRequestBody"
            responses:
                200:
                    description: "Wheel modified successfully"
    
    
    /complex-resource/v1/cars:
        post:
            tags:
            - "Car"
            operationId: "create_Car"
            
            parameters:
            - in: "body"
              name: "CarRequestBody"
              schema:
                $ref: "#/definitions/CarRequestBody"
            responses:
                200:
                    description: "Car created successfully"
    
        get:
            tags:
            - "Car"
            operationId: "multiget_Car"
            
            parameters:
               - in: "query"
                 description: "Page offset, starting from 1"
                 name: page
                 required: false
                 type: "integer"
                 format: "int32"
                 
               - in: "query"
                 description: "Size of returned page"
                 name: page-size
                 type: "integer"
                 format: "int32"             
    
            responses:
                200:
                    description: "Cars retrieved successfully"
                    schema:
                          $ref: "#/definitions/CarMultiResponse"
    
    
    /complex-resource/v1/cars/{id}:
        get:
            tags:
            - "Car"
            operationId: "retrieve_Car"
            description: "This is a GET comment override"
            
            parameters:
            - in: "path"
              name: "id"
              required: true
              type: "integer"
              format: "int32"
              
            responses:
                200:
                    description: "Car retrieved successfully"
                    schema:
                        $ref: "#/definitions/CarBody"
    
    
    
    
definitions:
        CarRequestBody:
            type: object
            properties:
                today:
                    type: "string"
                    example: "2019-04-13 (date)"
                    
                brand:
                    type: "enum"
                    
    
        CarBody:
            type: object
            properties:
                today:
                    type: "string"
                    example: "2019-04-13 (date)"
                    
                id:
                    type: "integer"
                    format: "int32"
                    
                brand:
                    type: "enum"
                    
    
        CarMultiResponse:
            type: object
            properties:
                 page:
                     description: "Page offset, starting from 1"
                     type: "integer"
                     format: "int32"
    
                 page-size:             
                     description: "Size of returned page"
                     type: "integer"
                     format: "int32"             
    
                 elements:
                     description: "Array of retrieved Cars"
                     type: array
                     items:
                        $ref: "#/definitions/CarBody"
        WheelRequestBody:
            type: object
            properties:
                radius:
                    type: "double"
                    
    
        WheelBody:
            type: object
            properties:
                id:
                    type: "integer"
                    format: "int32"
                    
                radius:
                    type: "double"
                    
    
        WheelMultiResponse:
            type: object
            properties:
                 page:
                     description: "Page offset, starting from 1"
                     type: "integer"
                     format: "int32"
    
                 page-size:             
                     description: "Size of returned page"
                     type: "integer"
                     format: "int32"             
    
                 elements:
                     description: "Array of retrieved Wheels"
                     type: array
                     items:
                        $ref: "#/definitions/WheelBody"
        SpecificationRequestBody:
            type: object
            properties:
                type:
                    type: "string"
                    
    
        SpecificationBody:
            type: object
            properties:
                type:
                    type: "string"
                    
    
        SpecificationMultiResponse:
            type: object
            properties:
                 page:
                     description: "Page offset, starting from 1"
                     type: "integer"
                     format: "int32"
    
                 page-size:             
                     description: "Size of returned page"
                     type: "integer"
                     format: "int32"             
    
                 elements:
                     description: "Array of retrieved Specifications"
                     type: array
                     items:
                        $ref: "#/definitions/SpecificationBody"
    
    
