package com.liveramp.uml2swagger;

public class Helpers {

	/**
	 * turn a string from possible camel case into hyphenated
	 * @param str input string
	 * @return output as lower case hyphenated
	 */
	public static String sanitize(String str) {
		StringBuilder sb = new StringBuilder();
		boolean empty = true;
		for (char ch : str.toCharArray()) {
			if (ch >= 'A' && ch <= 'Z') {
				if (!empty) {
					sb.append('-');
				}
			}
			sb.append(Character.toLowerCase(ch));
			empty = false;
		}
		return sb.toString();
	}
}
