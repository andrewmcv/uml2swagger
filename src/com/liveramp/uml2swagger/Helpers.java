package com.liveramp.uml2swagger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Property;

public class Helpers {

	/**
	 * turn a string from possible camel case into hyphenated
	 * @param str input string
	 * @return output as lower case hyphenated
	 */
	public static String sanitize(String str) {
		StringBuilder sb = new StringBuilder();
		boolean empty = true;
		for (char ch : str.toCharArray()) {
			if (ch >= 'A' && ch <= 'Z') {
				if (!empty) {
					sb.append('-');
				}
			}
			sb.append(Character.toLowerCase(ch));
			empty = false;
		}
		return sb.toString();
	}
	
	public static String pluralize(String str) {
	    if (str.endsWith("ss")) {
	    	return str + "es";
	    }
	    else if (str.endsWith("y")) {
	    	return str.substring(1, str.length()-1) + "ies";
	    }
        return str + "s";
	}
	
	public static String sanitizeAndPluralize(String str) {
		return pluralize(sanitize(str));
	}
	
	public static Class parent(Class c, AggregationKind kind) {
		Class other = c;
		for (Association assoc : c.getAssociations()) {
			boolean found = false;

			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() == kind &&
						p.getType() == c) {
					found = true;
				}
			}

			if (found) {
				for (Property p : assoc.getOwnedEnds()) {
					if (p.getType() != c) {
						other = (Class) p.getType();
					}
				}
			}
		}
		return other;
	}
	
	public static Boolean isSingleton(Class c) {
		boolean found = false;
		Property other = null;
		for (Association assoc : c.getAssociations()) {
			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() == AggregationKind.SHARED_LITERAL &&
						p.getType() == c) {
					found = true;
					other = p;
				}
			}
		}
		if (found) {
			return !other.isMultivalued();
		}
		return false;
	}
	
	public static Collection<Class> subresources(Class c) {
		ArrayList<Class> all = new ArrayList<>();
		for (Association assoc : c.getAssociations()) {
			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() == AggregationKind.SHARED_LITERAL && p.getType() != c) {
					all.add((Class) p.getType());
				}
			}
		}
		return all;
	}

	public static Collection<Class> verbs(Class c) {
		ArrayList<Class> all = new ArrayList<>();
		for (Association assoc : c.getAssociations()) {
			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() == AggregationKind.NONE_LITERAL && p.getType() != c) {
					Class pc = (Class) p.getType();
					if (isVerb(pc)) {
						all.add(pc);
					}
				}
			}
		}
		return all;
	}

	public static Collection<Class> contained(Class c) {
		ArrayList<Class> all = new ArrayList<>();
		for (Association assoc : c.getAssociations()) {
			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() == AggregationKind.COMPOSITE_LITERAL && p.getType() != c) {
					all.add((Class) p.getType());
				}
			}
		}
		return all;
	}

	public static Collection<Property> getStructureProperties(Class c) {
		ArrayList<Property> all = new ArrayList<>();
		for (Association assoc : c.getAssociations()) {
			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() == AggregationKind.COMPOSITE_LITERAL && p.getType() != c) {
					all.add(p);
				}
			}
		}
		return all;
	}
	
	
	public static Boolean isVerb(Class c) {
		String name = c.getName();
		Matcher match = VER_MATCH.matcher(name);
		if (match.matches()) {
			name = match.group(1);
		}
		return name.endsWith("Verb");
	}
	
	public static Collection<Property> getIdProperties(Class c) {
		ArrayList<Property> all = new ArrayList<>();
		for (Association assoc : c.getAssociations()) {
			boolean pure = true;
			Property end = null;
			for (Property p : assoc.getOwnedEnds()) {
				if (p.getAggregation() != AggregationKind.NONE_LITERAL) {
					pure = false;
				}
				if (p.getType() != c && p.isNavigable() && !isVerb((Class) p.getType())) {
					end = p;
				}
			}
			if (pure && end != null) {
				all.add(end);
			}
		}
		return all;
	}
	
	private static Pattern VER_MATCH = Pattern.compile("(.*)-v([0-9])+$");
	public static String removeVersion(Class c, Boolean addToURL, Boolean pluralize) {
		String version = getTag(c, "VERSION");
		if (version == null) {
			version = "1";
		}
		
		String name = c.getName();
		Matcher match = VER_MATCH.matcher(name);
		if (match.matches()) {
			name = match.group(1);
			version = match.group(2);
		}
		return (addToURL ? "v" + version + "/" : "") +
				(pluralize ? sanitizeAndPluralize(name) : sanitize(name));
	}
	
	public static String prettifyVersion(Class c, Boolean addVersion) {
		String version = getTag(c, "VERSION");
		if (version == null) {
			version = "1";
		}
		
		String name = c.getName();
		Matcher match = VER_MATCH.matcher(name);
		if (match.matches()) {
			name = match.group(1);
			version = match.group(2);
		}
		return name + (!version.equals("1") && addVersion ? " v" + version : "");
	}
	
	public static String removeVerb(String name) {
		if (name.endsWith("-verbs")) {
			return name.substring(0, name.length() - 6);
		}
		return name;
	}
	
	public static String getTag(NamedElement n, String tag) {
		EAnnotation ann = n.getEAnnotation("genmymodel");
		return ann == null ? null : ann.getDetails().get(tag);
	}
}
